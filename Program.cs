﻿using System;

namespace exam
{
    class Program
    {
        static void Main(string[] args)
        {
            int numero;
            Console.WriteLine("------------------------------------------");
            Console.WriteLine("Bienvenidos al cajero automatico del banco FDP INVERSMENTS");
            Console.WriteLine("El cajero tiene limites.");
            Console.WriteLine("Los limites son:");
            Console.WriteLine("Solo puede retirar 20,000 billetes si es del banco FDP INVERSMENTS");
            Console.WriteLine("De lo contrario, solo podra retirar 10,000 Billetes.");
            Console.WriteLine("De que banco es usted?");
            do{
            Console.WriteLine("Para retirar dinero, primero seleccione su banco.");
            Console.WriteLine("Digite el numero que tiene delante su banco.");
            Console.WriteLine("1. FDP INVERSMENTS");
            Console.WriteLine("2. Cualquier otro.");
            Console.WriteLine("3. Salir del sistema");
            numero = int.Parse(Console.ReadLine());
            Console.WriteLine("------------------------------------------");
            /*A continuacion, usare las letras V, c, y B;
            cada una significa lo siguiente:
            V = Valor de la variable.
            C = Cantidad de billetes que queda.
            B = Billetes con lo que se pagara.
            */

            int cbd500 = 19;
            int cbd200 = 23;
            int cbd100 = 50;
            int bdp1000 = 0;
            int bdp500 = 0;
            int bdp200 = 0;
            int bdp100 = 0;
            switch(numero){
                case 1: 
                        
                        Console.WriteLine("Ha escogido FDP INVERSMENTS");
                        Console.Write("Digite el monto que quiere retirar:");
                        int monto = int.Parse(Console.ReadLine());
                        if (monto >= 20000){
                            Console.WriteLine("Su monto a retirar a excedido el limite establecido '20,000' ");
                        }
                        else{
                            Console.WriteLine("Se les daran los siguientes billetes:");
                            while (monto != 0){
                                if (monto >= 19000){
                                    monto -= 18000;
                                    if(cbd500 != 0 ){
                                            bdp500=(monto-monto%500)/500;
                                            monto=monto%500;
                                        }
                                        if(cbd200 != 0){
                                            bdp200=(monto-monto%200)/200;
                                            monto=monto%200;
                                        }
                                        if(cbd100 != 0){
                                            bdp100=(monto-monto%100)/100;
                                            monto=monto%100;
                                        }
                                    Console.WriteLine("Usted ha recibido: 18 billetes de 1000");
                                    Console.WriteLine("Usted ha recibido: " + bdp500 + " billetes de 500");
                                    Console.WriteLine("Usted ha recibido: " + bdp200 + " billetes de 200");
                                    Console.WriteLine("Usted ha recibido: " + bdp100 + " billetes de 100");
                                }
                                else{
                                if (bdp1000 < 18){
                                    bdp1000=(monto-monto%1000)/1000;
                                    monto=monto%1000;
                                    }
                                        if(cbd500 != 0 ){
                                            bdp500=(monto-monto%500)/500;
                                            monto=monto%500;
                                        }
                                        if(cbd200 != 0){
                                            bdp200=(monto-monto%200)/200;
                                            monto=monto%200;
                                        }
                                        if(cbd100 != 0){
                                            bdp100=(monto-monto%100)/100;
                                            monto=monto%100;
                                        }
                                    Console.WriteLine("Usted ha recibido: " + bdp1000 + " billetes de 1000");
                                    Console.WriteLine("Usted ha recibido: " + bdp500 + " billetes de 500");
                                    Console.WriteLine("Usted ha recibido: " + bdp200 + " billetes de 200");
                                    Console.WriteLine("Usted ha recibido: " + bdp100 + " billetes de 100");
                                    }
                                }

                                
                        }
                        break;
                case 2: 
                        Console.WriteLine("Ha escogido 'OTRO'");
                        Console.Write("Digite el monto que quiere retirar:");
                        int monto2 = int.Parse(Console.ReadLine());
                        if (monto2 >= 10000){
                            Console.WriteLine("Su monto a retirar a excedido el limite establecido '10,000' ");
                        }
                        else{
                            Console.WriteLine("Se les daran los siguientes billetes:");
                            if (bdp1000 < 18){
                            bdp1000=(monto2-monto2%1000)/1000;
                                monto2=monto2%1000;
                                    }
                                        if(cbd500 != 0 ){
                                            bdp500=(monto2-monto2%500)/500;
                                            monto2=monto2%500;
                                        }
                                        if(cbd200 != 0){
                                            bdp200=(monto2-monto2%200)/200;
                                            monto2=monto2%200;
                                        }
                                        if(cbd100 != 0){
                                            bdp100=(monto2-monto2%100)/100;
                                            monto2=monto2%100;
                                        }
                                    Console.WriteLine("Usted ha recibido: " + bdp1000 + " billetes de 1000");
                                    Console.WriteLine("Usted ha recibido: " + bdp500 + " billetes de 500");
                                    Console.WriteLine("Usted ha recibido: " + bdp200 + " billetes de 200");
                                    Console.WriteLine("Usted ha recibido: " + bdp100 + " billetes de 100");
                        }
                    break;
                case 3: 
                        Console.WriteLine("Gracias por usar nuestro sistema.");
                        Console.WriteLine("Vuelva a preferirnos!.");
                        Console.WriteLine("CREADO TOTALMENTE POR ALEXANDER.");
                        break;
            }
            Console.WriteLine("------------------------------------------");
            }while (numero!=3);

        }
    }
}

//By Alexander, para el examen de c#.
//Espero que le agrade profe!
